#!/bin/bash

baseurl='https://apibk.cliento.com/api/v2/partner/cliento'
clientId='4mfh5wH5yJpGVZjk80z50D'

fromDate=$(date +%F)
a=$(date +%s)
((a += 4*7*24*3600))

test $(uname) = Linux && printf -v toDate "%(%F)T" $a || toDate=$(date -j -f "%s" "$a" "+%F")

echo "Looking for slots between $fromDate and $toDate ..."

for srvIds in $(curl -s $baseurl/$clientId/ref-data/ | jq '.services[].serviceId')
    do 
    jqarg=$(echo ".services[] | select(.serviceId == $srvIds ) | .group")
    group=$(curl -s $baseurl/$clientId/ref-data/ | jq "$jqarg")
    echo $group:
    curl -s "${baseurl}/${clientId}/resources/slots?fromDate=${fromDate}&srvIds=${srvIds}&toDate=${toDate}" | jq '.'
done
