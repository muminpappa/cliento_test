## REST API of bokamera.se (for VaccinDirekt)
Swagger: https://testapi.bokamera.se/swagger-ui/index.html

Order an API key at https://bokamera.se

VaccinDirekt Lund Kalkstensvägen has the Id `f768b6b2-0455-46b2-8084-d3a77a40e31f`

Their API key seems to be `4F4DEE5D-FC5F-496E-8011-82F56311BEEA`

53EECA6F-9EDA-49CF-8832-7DEF10E355CF

apiBaseUrl is https://api.bokamera.se/resources 

https://docs.servicestack.net/jwt-authprovider

API is quite locked down.

Get the service groups:
```sh
curl -H "x-api-key: 4F4DEE5D-FC5F-496E-8011-82F56311BEEA" -H "Content-Type: application/json" "https://api.bokamera.se:443/services/grouped?CompanyId=f768b6b2-0455-46b2-8084-d3a77a40e31f" | jq '.'
```


`ServiceId=88382`

```sh
curl -H "x-api-key: 4F4DEE5D-FC5F-496E-8011-82F56311BEEA" -H "Content-Type: application/json" "https://api.bokamera.se:443/services/88382/availabletimes?CompanyId=f768b6b2-0455-46b2-8084-d3a77a40e31f&From=2021-05-24T00%3A00%3A00&To=2021-05-28T00%3A00%3A00&&&&" | jq .
```

The API list these slots but it's impossible to book them. How can I see that they are blocked? 

## Explore the REST API of cliento.com (for vaccina.se)

Get the client Id from the web page source https://www.vaccina.se/covidvaccin/skane/tidsbokning/#/calendar 

Documentation of the cliento REST API:

https://developers.cliento.com/docs/rest-api/


curl https://cliento.com/api/v2/partner/cliento/4mfh5wH5yJpGVZjk80z50D/settings/

curl https://cliento.com/api/v2/partner/cliento/4mfh5wH5yJpGVZjk80z50D/ref-data/ | jq


List all resources:

    curl https://cliento.com/api/v2/partner/cliento/4mfh5wH5yJpGVZjk80z50D/ref-data/ | jq '.resources[] | {"id": .id, "name": .name}'

List services:

    curl https://cliento.com/api/v2/partner/cliento/4mfh5wH5yJpGVZjk80z50D/ref-data/ | jq '.services[] | {"group": .group, "serviceId": .serviceId}'

The mapping between services and resources:

    curl https://cliento.com/api/v2/partner/cliento/4mfh5wH5yJpGVZjk80z50D/ref-data/ | jq '.resourceMappings'

List all resources for the given service ID 30009 (sorry, very dirty):

    curl https://cliento.com/api/v2/partner/cliento/4mfh5wH5yJpGVZjk80z50D/ref-data/ | jq '.resourceMappings' | grep ':30009' | cut -d : -f 1 | cut -d '"' -f 2

    curl https://cliento.com/api/v2/partner/cliento/4mfh5wH5yJpGVZjk80z50D/ref-data/ | jq '.resourceMappings | keys'



List the slots for a given service ID and resource ID:

    curl 'https://cliento.com/api/v2/partner/cliento/4mfh5wH5yJpGVZjk80z50D/resources/slots?fromDate=2021-05-18&resIds=5773&srvIds=30009&toDate=2021-06-07'


The kind support at Cliento.com recommended to use Web Inspector instead of the API documentation.
Seems like the `resIds` argument is not needed, and that the correct base URL is apibk.cliento.com:

    curl 'https://apibk.cliento.com/api/v2/partner/cliento/4mfh5wH5yJpGVZjk80z50D/resources/slots?fromDate=2021-05-17&srvIds=30009&toDate=2021-05-23'